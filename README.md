# THSF.NET

Le site du **THSF**

## Fonctionnement du site

Le site du **THSF** est une application *Python Flask* configurée à partir du fichier `config.yml` situé à la racine du dépôt.

Il n'est généralement pas nécessaire de modifier ce fichier de configuration, cependant il ets possible d'augmenter le niveau de *log* en modifiant la clef de configuration `log::root::level` en la passant à `DEBUG` (valeur en production: `INFO`).

### Conteneur

Un fichier `thsf.Dockerfile` ainsi que le fichier `docker-compose.yml` correspondant permettront de rapidement démarrer localement une instance du site du **THSF**.

### Démarrer une instance du site du **THSF**

Assurez vous d'avoir l'environnement de conteneurisation adéquat (i.e: *docker* + *docker-compose*) puis exécuter directement les commandes suivantes depuis la racine du dépôt:

```shell
docker-compose build
docker-compose up -d
```

Le site du **THSF** écoutera alors sur le port *TCP/8042*:

`http://127.0.0.1:8042`

### Arrêter une instance du site du **THSF**

```shell
docker-compose down
```

## Apporter des modifications au site

**Important**: Avant d'apporter la moindre modification, pensez à créer une nouvelle branche à l'aide de la commande suivante en remplaçant `ma_branche` par le nom de votre branche:

```shell
git checkout -b ma_branche
```

Le code de l'application *Python Flask* se situe dans le répertoire `src` et une fois votre branche créée, vous pouvez apporter toutes les modification souhaitées.

### Créer un patch contenant toutes vos modifications

Une fois satisfaits de vos modifications vous pouvez créer un *patch* regroupant l'ensemble de vos modifications à l'aide la commande suivante

```shell
git diff -u --patch  master..ma_branche > mes_modifications.patch
```

Vous pouvez faire parvenir votre fichier de *patch* à **contact@tetalab.org** pour que vos modifications soient incorporées au site après vérification.

### Publication automatisée des modifications

**Note**: Cette partie ne concerne que les personnes ayant un accès en écriture sur le dépôt *Git*.

Il n'est **pas possible** de pousser directement des modifications sur la branche `master`.

Pour modifier le site, il est nécessaire de créer une branche spécifique et d'y pousser vos modifications.

Lorsque vous êtes satisfaits de vos modifications, vous pouvez créer [une demande d'ajout](https://git.tetalab.org/tetalab/thsf.net/pulls) de votre branche sur la branche `master`.

Lorsque la demande de fusion sera acceptée, vos modifications seront automatiquement publiées sur [le site du THSF](https://www.thsf.net).

Vous pouvez auto-accepter vos demandes de fusion si vous faites partie des développeurs autorisés.
