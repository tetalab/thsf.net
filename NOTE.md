# API

## Récupérer l'évènement

```bash
curl -s -X GET -H "Authorization: Token <TOKEN>" https://23.thsf.net/api/events/
```

Réponse:

```json
[{"name":{"en":"Toulouse HackerSpace Factory","fr":"Toulouse HackerSpace Factory"},"slug":"thsf-2023","is_public":true,"date_from":"2023-05-26","date_to":"2023-05-28","timezone":"Europe/Paris","urls":{"base":"https://23.thsf.net/thsf-2023/","schedule":"https://23.thsf.net/thsf-2023/schedule/","login":"https://23.thsf.net/thsf-2023/login/","feed":"https://23.thsf.net/thsf-2023/schedule/feed.xml"}}]
```

## Récupérer la liste des intervenants

```bash
curl -s -X GET -H "Authorization: Token <TOKEN>" https://23.thsf.net/api/speakes/
```

## Récupérer la liste des salles

 curl -s -X GET -H "Authorization: Token <TOKEN>" https://23.thsf.net/api/events/thsf-2023/rooms/

## Récupérer les versions des plannings de l'évènement

```bash
curl -s -X GET -H "Authorization: Token <TOKEN>" https://23.thsf.net/api/events/<EVENT>/schedules/
```

Réponse:

```json
{"count":6,"next":null,"previous":null,"results":[{"version":"wip","published":null},{"version":"0.5","published":"2023-04-03T12:14:39.675671+02:00"},{"version":"0.4","published":"2023-04-03T10:07:41.632059+02:00"},{"version":"0.3","published":"2023-03-25T12:34:17.704819+01:00"},{"version":"0.2","published":"2023-03-25T12:21:23.464899+01:00"},{"version":"0.1","published":"2023-03-25T12:14:10.460100+01:00"}]}
```

### Récupérer un planning spécifique

```bash
curl -s -X GET -H "Authorization: Token <TOKEN>" https://23.thsf.net/api/events/thsf-2023/schedules/<version>/
```

Reponse:

```json
{
  "slots": [
    {
      "code": "JHCFVE",
      "speakers": [
        {
          "code": "BT9CDU",
          "name": "Youssr Youssef",
          "biography": "",
          "avatar": "https://23.thsf.net/media/avatars/YoussrYoussef_408QSEG.jpeg"
        }
      ],
      "title": "René Carmille, un hacker sous l'Occupation",
      "submission_type": {
        "en": "Screening",
        "fr": "Projection"
      },
      "track": null,
      "state": "confirmed",
      "abstract": "Entre collaboration et résistance, confronté aux premiers enjeux modernes de la collecte de données en masse, la trajectoire étonnante de René Carmille n'a encore jamais été racontée... Son héritage est pourtant majeur, les questions qu'il soulève le sont tout autant. \r\nSous l’Occupation, le polytechnicien René Carmille est le précurseur de l'usage massif des données en France : c'est lui qui a mis en place et dirigé le Service national de statistiques (SNS) qui deviendra plus tard l'INSEE, c'est aussi lui qui a inventé notre numéro de sécurité sociale.\r\n\r\nLa projection du documentaire sera suivie d'une rencontre en visioconférence avec la réalisatrice.",
      "description": "",
      "duration": 80,
      "slot_count": 1,
      "do_not_record": false,
      "is_featured": true,
      "content_locale": "fr",
      "slot": {
        "room": {
          "en": "Salle 1 Cinema",
          "fr": "Salle 1 Cinema"
        },
        "start": "2023-05-27T11:30:00+02:00",
        "end": "2023-05-27T12:50:00+02:00"
      },
      "image": "https://23.thsf.net/media/thsf-2023/submissions/JHCFVE/Carmille-1-scaled_p0gxA6D.jpeg",
      "resources": []
    },
...
```
