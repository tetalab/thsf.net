.PHONY: clean distclean install run stop all docker-build docker-run docker-stop docker-all

install:
	set -e ;\
	python -m venv thsf_venv; \
	. thsf_venv/bin/activate; \
	pip install -r requirements.txt ;\
	pip install . ;\
	pip install gunicorn;

run:
	set -e ;\
	THSF_DIR=$$(pwd); \
	. thsf_venv/bin/activate; \
	thsf_venv/bin/gunicorn -p thsf.pid -w 4 -b 0.0.0.0:8042 'thsf:app'

debug:
	set -e ;\
	THSF_DIR=$$(pwd); \
	. thsf_venv/bin/activate; \
	thsf_venv/bin/gunicorn -p thsf.pid -w 4 -b 0.0.0.0:8042 'thsf:app'

stop:
	-set -e;\
	THSF_DIR=$$(pwd); \
	kill -15 $$(cat $$THSF_DIR/thsf.pid); \
	rm thsf.pid

clean:
	-set -e ;\
	THSF_DIR=$$(pwd); \
	rm -Rf ./build ./src/thsf.egg-info ./thsf.pid ./thsf.log; \
	. thsf_venv/bin/activate; \
	pip uninstall thsf -y

distclean: clean
	rm -Rf thsf_venv/

all: stop clean install run

docker-build:
	docker-compose build

docker-run:
	docker-compose up -d

docker-stop:
	-docker-compose down

docker-all: docker-stop docker-build docker-run
