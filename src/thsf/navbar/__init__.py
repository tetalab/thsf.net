class Navbar:
  def __init__(self, config):
    self.config = config

  def get_from_page(self, page):
    return [item for item in self.config["items"] if item["url"] != page]
