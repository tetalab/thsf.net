import requests


class Backend:
  def __init__(self, url, apiprefix, apikey, ssl_verify):
    self.url = url
    self.apiprefix = apiprefix
    self.apikey = apikey
    self.ssl_verify = ssl_verify
    self.session = requests.Session()

  def get(self, endpoint, params=None):
    url = f"{self.url}/{self.apiprefix}/{endpoint}"
    headers = {"Authorization": f"Token {self.apikey}",
               "Accept": "application/json"}
    return self.session.get(url,
                            params=params,
                            headers=headers,
                            verify=self.ssl_verify)
