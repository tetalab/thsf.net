#!/usr/bin/python3
import docker
import jinja2

base_path = "/var/www/www.thsf.net/thsf.net/maintenance/templates"
status_template = "status.html"

env = jinja2.Environment(loader=jinja2.FileSystemLoader(base_path),
                         autoescape=True)
template = env.get_template(status_template)

client = docker.from_env()
for container in client.containers.list():
  if container.name == "thsf":
    cont = dict()
    cont["name"] = container.name
    cont["status"] = container.status
    cont["logs"] = container.logs().decode('utf-8').replace('\n', '<br>')
    print(template.render(containers=[cont]))
