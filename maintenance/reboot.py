#!/usr/bin/python3
import docker

client = docker.from_env()
for container in client.containers.list():
  if container.name == "thsf":
    print("Content-type: text/html\n\n")
    print("[+] Rebooting container<br>")
    print(" '-> Stopping container<br>")
    container.stop()
    print(" '-> Starting container<br>")
    container.start()
    print("[+] Container restarted<br>")
