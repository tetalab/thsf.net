ARG PRETALX_HOSTS_ENTRY
FROM python:3.9.16-bullseye as thsf_base
RUN apt-get update -y && \
    apt-get install -y bash make python3-pip && \
    useradd -m -d /home/thsf -s /bin/bash thsf

from thsf_base as thsf_build
COPY . /home/thsf/
RUN chown -R thsf:thsf /home/thsf/
USER thsf
WORKDIR /home/thsf/

from thsf_build
RUN	make install
CMD ["make", "run"]
